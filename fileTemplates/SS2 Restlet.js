#parse("ss2.header.js")
     
     * @NScriptType RESTlet
     */
    var exports = {};

#if ($get == 'y')
#parse("ss2.restlet.get.js")
#end

#if ($post == 'y')
#parse("ss2.restlet.post.js")
#end

#if ($put == 'y')
#parse("ss2.restlet.put.js")
#end

#if ($delete == 'y')
#parse("ss2.restlet.delete.js")
#end

#if ($get == 'y')
    #parse("ss2.restlet.get.export.js")
#end
#if ($post == 'y')
    #parse("ss2.restlet.post.export.js")
#end
#if ($put == 'y')
    #parse("ss2.restlet.put.export.js")
#end
#if ($delete == 'y')
    #parse("ss2.restlet.delete.export.js")
#end
#parse("ss2.footer.js")
