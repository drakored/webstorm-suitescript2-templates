    /**
     * <code>beforeUpdate</code> event handler
     * 
     * @gov XXX
     * 
     * @param params
     * 		{Object}            
     * @param params.fromVersion
     * 		{Number} The version of the bundle that is currently installed
     * @param params.toVersion
     * 		{Number} The version of the bundle that is being installed
     * 
     * @return {void}
     * 
     * @static
     * @function beforeUpdate
     */
    function beforeUpdate(params) {
        // TODO
    }
