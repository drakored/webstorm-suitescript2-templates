#parse("ss2.header.js")
     
     * @NScriptType UserEventScript
     */
    var exports = {};

#if ($beforeLoad == 'y')
#parse("ss2.ue.beforeLoad.js")
#end

#if ($beforeSubmit == 'y')
#parse("ss2.ue.beforeSubmit.js")
#end

#if ($afterSubmit == 'y')
#parse("ss2.ue.afterSubmit.js")
#end

#if ($beforeLoad == 'y')
    #parse("ss2.ue.beforeLoad.export.js")
#end
#if ($beforeSubmit == 'y')
    #parse("ss2.ue.beforeSubmit.export.js")
#end
#if ($afterSubmit == 'y')
    #parse("ss2.ue.afterSubmit.export.js")
#end
#parse("ss2.footer.js")
