    /**
     * <code>getInputData</code> event handler
     * 
     * @gov XXX
     * 
     * @return {*[]|Object|Search|ObjectRef} Data that will be used as input for
     *         the subsequent <code>map</code> or <code>reduce</code>
     * 
     * @static
     * @function getInputData
     */
    function getInputData() {
        // TODO
        #if ($parameterized == 'y')
        var params = readParameters(runtime.getCurrentScript());
    
        return {
            type: "search|file",
            id: params.$paramKey
            path: ""
        };
        #end
    }
