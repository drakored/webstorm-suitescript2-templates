    /**
     * <code>summarize</code> event handler
     * 
     * @gov XXX
     * 
     * @param summary
	 * 		{Summary} Holds statistics regarding the execution of a map/reduce
	 * 			script
     * 
     * @return {void}
     * 
     * @static
     * @function summarize
     */
    function summarize(summary) {
        if (summary.inputSummary.error) {
            log.error({title: "getInputData Error:", details: summary.inputSummary.error});
        }

        [summary.mapSummary.errors, summary.reduceSummary.errors].forEach(function (smy) {
            smy.iterator().each(function (key, e) {
                log.error({title: e.name, details: e.message});
            });
        });
        
        // TODO
    }
