    /**
     * <code>onRequest</code> event handler
     * 
     * @gov XXX
     *
     * @param context
	 * 		{Object}            
     * @param context.request
	 * 		{ServerRequest} The incoming request object
     * @param context.response
	 * 		{ServerResponse} The outgoing response object
     * 
     * @return {void}
     * 
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        log.audit({title: "Request received"});

        var eventRouter = {};
        #if ($get == "y")
        eventRouter[https.Method.GET] = onGet;
        #end
        #if ($post == "y")
        eventRouter[https.Method.POST] = onPost;
        #end

        try {
            (eventRouter[context.request.method])(context);
        } catch (e) {
            onError({context: context, error: e});
        }

        log.audit({title: "Request complete"});
    }
